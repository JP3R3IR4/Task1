# TASK1 
Objetivo: Controlar uma placa ChipKIT Uno32 através do JTAG bus  


## Conteúdo

- [Conceito do Sistema](#Conceito-do-Sistema)
- [Requisitos](#Requisitos)
- [Controlo JTAG](#Controlo-JTAG)
    - [IEEE 1149.1 TAP controller](#IEEE-1149.1-TAP-controller)
    - [Ler o ID Code](#Ler-o-ID-Code)
    - [Ligar/Desligar o LED](#Ligar/Desligar-o-LED)
    - [Ler o Estado do botão](#Ler-o-Estado-do-botão)

## Conceito do Sistema

![SC](https://user-images.githubusercontent.com/61596101/102628690-ff7d1800-4141-11eb-982c-abb6c4c36352.png)

## Requisitos

- d – Gets the ID CODE of the PIC32 and prints it in hexadecimal code
- 1 – Turn on LED LD5 of the ChipKIT board
- 0 – Turn off LED LD5 of the ChipKIT board
- b – Print the state of the button connected to pin 29 of the ChipKIT 


## Controlo JTAG

JTAG pins utilizados:
- TCK (Test Clock)
- TMS (Test Mode Select)
- TDI (Test Data In)
- TDO (Test Data Out)

#### IEEE 1149.1 TAP controller

Para controlar o PIC32 através dos JTAG ports é necessário controlar o IEEE 1149.1 TAP controller state diagram para realizar todas as funcionalidades requeridas 
Com isto foi desenvolvida uma função que progredisse no diagrama de estados de forma a ativar o estado pretendido para determinada funcionalidade:

````
void TAPstate(uint8_t state, uint8_t size);
````

Esta função recebe state que é um valor binário pré-definido com os bits que levam o programa do estado atual ao estado pretendido. (size será o número de iterações necessárias até chegar a esse novo estado)

````
//TAP Controller States
#define RESET    0b11111   // -> RESET
#define SHIFT_IR 0b00110   //Reset -> SHIFT-IR
#define SHIFT_DR 0b0010    //Reset/IDLE -> SHIFT-DR
#define IDLE     0b011     //EXIT_IR/DR -> IDLE
````

Com esta função torna-se mais fácil chegar aos estados necessários, quer para introduzir uma instrução, quer para enviar ou receber dados através de TDI e TDO

#### Ler o ID Code

Para receber o ID Code referente à placa é necessário:
- Fazer um reset no diagrama para ser possível inicializar à máquina de estados.
- Progredir até ao estado SHIFT_IR para introduzir a instrução pretendida.
- Voltar ao estado Idle da máquina de estados.
- Progredir até ao estado SHIFT_DR para iniciar o envio e receção de bits.
Esta **lógica inicial** é comum a todas as operações realizadas neste sitema.

````
  TAPstate(RESET, 5);
  TAPstate(SHIFT_IR, 5);
  instruction(IDCODE);
  TAPstate(IDLE>>1, 2);
  
  TAPstate(SHIFT_DR, 4);
````

De seguida estamos posicionados corretamente para realizar a operação desejada. 
Neste caso para receber os bits referentes ao IDCode é necessário apenas "shiftar" do PIC32 32 bits que serão os bits do IDCode e guardá-los. Para isso lê-se o pin TDI do arduino que se refere ao pin TDO do PIC32 à medida que em TDO(arduino)/TDI(PIC32) se colocam 0s por exemplo e se faz o shift dos bits controlando o TCK  

 #### Ligar/Desligar o LED

Utilizando a **lógica inicial** referida a cima é possíel colocar o diagrama de estados posicionado no estdao pretendido de modo a realizar a tarefa pretendida.
Neste caso a instrução introduzida em SHIFT_IR seria EXTEST de modo a aceder a todo o vetor de registos JTAG do PIC32.
Com isto o objetivo é alterar esses registos de modo a realizar a operação de ligar ou desligar o led.
Tendo em atenção os seguintes pins:

````
#define LED_OUT  19
#define LED_CONT 20
#define MCLR     129
````

Será necessário apenas colocar a 1 os pins referentes a LED_CONT (abilita o pin como output) e MCLR (realiza um reset global quando vai a 0).
Colocar no estado pretendido ON(1)/OFF(0) o pin LED_OUT que controlará o led da placa que se pretende controlar

 #### Ler o Estado do botão

Aqui da mesma forma utiliza-se a **lógica inicial** com a intrução de SAMPLE faz-se o "shift" para TDI do valor do registo do pin 3 (do botão), lê-se o valor do bit e imprime-se na porta UART o valor lógico correspondente. 
 
