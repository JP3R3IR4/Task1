/*
 * 
 * Author: Joao Pereira  (up201909554@fe.up.pt)
 * 		   Roberto Lopes (up201606445@fe.up.pt)
 * 
 * University: FEUP MIEEC SELE
 * 
 * Date: 15/12/2020
 * 
 * 
 */

#include <Arduino.h>
#include <JTAG_control.c>

#define LED_OUT  19
#define LED_CONT 20
#define MCLR     129
#define BUT_REG  3
#define BAUD     9600

uint32_t getIDcode() {

  uint32_t IDcode = 0;

  TAPstate(RESET, 5);
  TAPstate(SHIFT_IR, 5);
  instruction(IDCODE);
  TAPstate(IDLE>>1, 2);

  TAPstate(SHIFT_DR, 4);
  IDcode = tdi(32);
  TAPstate(IDLE, 3);

  return IDcode;
}

void setLed(uint8_t state) {

  TAPstate(RESET, 5);
  TAPstate(SHIFT_IR, 5);
  instruction(EXTEST);
  TAPstate(IDLE>>1, 2);

  TAPstate(SHIFT_DR, 4);
  for (int i = 0; i < 147; i++) {
    if(i == LED_OUT) {
      tdo(state);
      tms(0);
    }
    else if(i == LED_CONT || i == MCLR) {
      tdo(1);
      tms(0);
    }
    else {
      tdo(0);
      tms(0);
    }
  }
  TAPstate(IDLE, 3);
}

boolean getButton() {

  uint32_t state = 0;

  TAPstate(RESET, 5);
  TAPstate(SHIFT_IR, 5);
  instruction(SAMPLE);
  TAPstate(IDLE>>1, 2);

  TAPstate(SHIFT_DR, 4);
  for (int i = 0; i < BUT_REG; i++) {
    tms(0);
    tdo(0);
  }
  state = digitalRead(TDI);

  if((state & 0x00000001) == 0x00000001) {
    TAPstate(IDLE, 3);
    return HIGH;
  }
  else {
    TAPstate(IDLE, 3);
    return LOW;
  }
}

void setup() {

  Serial.begin(BAUD);

  pinMode(TMS, OUTPUT);
  pinMode(TCK, OUTPUT);
  pinMode(TDO, OUTPUT);
  pinMode(TDI, INPUT);

  Serial.println("Task1 Test");
  
}

void loop() {
  if(Serial.available() > 0) {
    switch (Serial.read())
    {
    case 'D':  
    case 'd':
      Serial.print("IDCODE: ");
      Serial.println(getIDcode(), HEX);
      break;

    case '1':
      Serial.println("LED ON");
      setLed(1);
      break;

    case '0':
      Serial.println("LED OFF");
      setLed(0);
      break;

    case 'B':
    case 'b':
      if(!getButton()) Serial.println("BUTTON PRESSED");
      else             Serial.println("BUTTON RELEASED");
      break;

    default:
      Serial.println("Not Available");
      break;
    }
  }
}

