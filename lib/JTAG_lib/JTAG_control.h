/*
 * 
 * Author: Joao Pereira  (up201909554@fe.up.pt)
 * 		   Roberto Lopes (up201606445@fe.up.pt)
 * 
 * University: FEUP MIEEC SELE
 * 
 * Date: 15/12/2020
 * 
 * 
 */

//JTAG Test Pins
#define TMS 8   //Mode Select Pin
#define TCK 9   //Clock Input Pin
#define TDI 10  //Data Input Pin
#define TDO 11  //Data Otput Pin

//JTAG Instructions
#define IDCODE 0x01 
#define EXTEST 0x06 
#define SAMPLE 0x02 

//TAP Controller States
#define RESET    0b11111   // -> RESET
#define SHIFT_IR 0b00110   //Reset -> SHIFT-IR
#define SHIFT_DR 0b0010    //Reset/IDLE -> SHIFT-DR
#define IDLE     0b011     //EXIT_IR/DR -> IDLE

void tck();
void tms(uint8_t i);
uint8_t tdo(uint8_t i);
uint32_t tdi(uint8_t size);
void TAPstate(uint8_t state, uint8_t size);
void instruction(uint8_t instruction);



