/*
 * 
 * Author: Joao Pereira  (up201909554@fe.up.pt)
 * 		   Roberto Lopes (up201606445@fe.up.pt)
 * 
 * University: FEUP MIEEC SELE
 * 
 * Date: 15/12/2020
 * 
 * 
 */

#include <Arduino.h>
#include <util/delay.h>
#include "JTAG_control.h"

void tck() {
    digitalWrite(TCK, HIGH);
	_delay_us(10);
	digitalWrite(TCK, LOW);
	_delay_us(10);
}

void tms(uint8_t i) { 
    digitalWrite(TMS, i);
}

uint8_t tdo(uint8_t i) { 
    digitalWrite(TDO, i);
    tck();
    return digitalRead(TDI);
}

uint32_t tdi(uint8_t size) {
    uint32_t data = 0;
    for (int i = 0; i < size; i++) {
        data |= (uint32_t) digitalRead(TDI) << i;
        //data |= (uint32_t)tdo(0) << i;
        tms(0);
        tdo(0);
    }
    return data;
}

void TAPstate(uint8_t state, uint8_t size) {
    for (int i = 0; i < size; i++) {
        tms(state & (1 << i));
        tdo(0);
    }
}

void instruction(uint8_t instruction) {
    for (int i = 0; i < 5; i++) {
        tms(0b10000 & (1 << i));
        tdo(instruction & (1 << i));
    }
}

